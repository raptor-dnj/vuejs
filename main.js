var personList = [
    { id: 1, name: 'Touhid', age: 30},
    { id: 2, name: 'Lisa', age: 23},
    { id: 3, name: 'Sanjida', age: 16},
    { id: 4, name: 'Ruhana', age: 4},
];


const app = Vue.createApp({
    data(){
        return {
            cart: 0,
            mysize: 16,
            firstName: "Tohidul",
            lastName: "Islam",
            designation: "Programmer",
            age: 30,
            salary: 20000,
            inStock: false,
            stock: 0,
            persons: personList,
            bgColor: 'gray',
        }
    },
    methods: {
        addToCart(){
            this.cart  += 1;
        },
        addFontSize(){
            this.mysize += 1;
        }
      
    }
});